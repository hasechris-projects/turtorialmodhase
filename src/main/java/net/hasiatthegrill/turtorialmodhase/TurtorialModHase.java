package net.hasiatthegrill.turtorialmodhase;

import net.hasiatthegrill.turtorialmodhase.lists.BlockList;
import net.hasiatthegrill.turtorialmodhase.lists.ItemList;
import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(TurtorialModHase.MODID)
public final class TurtorialModHase {

    public static TurtorialModHase instance;
    public  static final String MODID = "turtorialmodhase";
    private static final Logger logger = LogManager.getLogger(MODID);

    public TurtorialModHase() {
        //LOGGER.debug("Hello from TurtorialModHase!");
        instance = this;

        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);

        MinecraftForge.EVENT_BUS.register(this);

    }

    private void setup(final FMLCommonSetupEvent event)
    {
        logger.info(TurtorialModHase.MODID + ": Setup method registered");

    }

    private void clientRegistries(final FMLClientSetupEvent event)
    {
        logger.info(TurtorialModHase.MODID + ": clientRegistries method registered");
    }

    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)

    public static class RegistryEvents
    {
        @SubscribeEvent
        public static void registerItems(final RegistryEvent.Register<Item> event)
        {
            event.getRegistry().registerAll(
                ItemList.turtorial_item = new Item(new Item.Properties().group(ItemGroup.MISC)).setRegistryName( location("turtorial_item")),

                ItemList.turtorial_block = new BlockItem(BlockList.turtorial_block, new Item.Properties().group(ItemGroup.MISC)).setRegistryName(BlockList.turtorial_block.getRegistryName())
            );
            logger.info("Items registered");
        }


        @SubscribeEvent
        public static void registerBlocks(final RegistryEvent.Register<Block> event)
        {
            event.getRegistry().registerAll(
                    BlockList.turtorial_block = new Block(Block.Properties.create(Material.IRON).hardnessAndResistance(2.0f, 3.0f).lightValue(15).sound(SoundType.ANVIL)).setRegistryName(location("turtorial_block"))
            );
            logger.info("Blocks registered");
        }


        private static ResourceLocation location(String name)
        {
            return new ResourceLocation(MODID, name);
        }
    }
}
